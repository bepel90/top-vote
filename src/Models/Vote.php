<?php

namespace Bepel90\TopVote\Models;

use Flarum\Database\AbstractModel;

class Vote extends AbstractModel
{
    protected $table = 'top_vote_votes';

    protected $fillable = [
        'fabricant_id',
        'user_id',
    ];

    public function fabricant()
    {
        return $this->belongsTo(Fabricant::class, 'fabricant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
