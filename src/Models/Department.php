<?php

namespace Bepel90\TopVote\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'top_vote_departments';

    public function fabricants()
    {
        return $this->hasMany(Fabricant::class, 'department_id');
    }
}
