<?php

namespace Bepel90\TopVote\Models;

use Flarum\Database\AbstractModel;
use Illuminate\Support\Str;

class Fabricant extends AbstractModel
{
    protected $table = 'top_vote_fabricants';

    protected $fillable = [
        'commercial_name',
        'company_name',
        'siret',
        'website',
        'image',
        'creation_date',
        'address',
        'phone',
        'email',
        'description',
        'status',
        'department_id',
    ];

    protected $dates = ['creation_date', 'added_date'];

    public $timestamps = true;

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function clicks()
    {
        return $this->hasMany(FabricantClick::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function getClickCountAttribute()
    {
        return $this->clicks()->count();
    }

    public function getVotesCountAttribute()
    {
        return $this->votes()->count();
    }

    public function getSlugAttribute()
    {
        return strtolower($this->id . '-' . Str::slug($this->commercial_name));
    }
}