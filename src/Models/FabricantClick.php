<?php

namespace Bepel90\TopVote\Models;

use Illuminate\Database\Eloquent\Model;

class FabricantClick extends Model
{
    protected $table = 'top_vote_fabricants_clicks';

    protected $fillable = [
        'fabricant_id',
        'ip_address'
    ];
}
