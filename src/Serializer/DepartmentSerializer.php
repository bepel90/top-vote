<?php

namespace Bepel90\TopVote\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;

class DepartmentSerializer extends AbstractSerializer
{
    protected $type = 'departments';

    protected function getDefaultAttributes($department)
    {
        return [
            'code' => $department->code,
            'name' => $department->name,
        ];
    }
}
