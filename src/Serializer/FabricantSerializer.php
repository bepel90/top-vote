<?php

namespace Bepel90\TopVote\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;
use Bepel90\TopVote\Models\Fabricant;

class FabricantSerializer extends AbstractSerializer
{
    protected $type = 'fabricants';

    protected function getDefaultAttributes($fabricant)
    {
        return [
            'id' => $fabricant->id,
            'commercial_name' => $fabricant->commercial_name,
            'company_name' => $fabricant->company_name,
            'siret' => $fabricant->siret,
            'website' => $fabricant->website,
            'image' => $fabricant->image,
            'created_at' => $fabricant->created_at,
            'address' => $fabricant->address,
            'phone_number' => $fabricant->phone,
            'added_at' => $fabricant->added_at,
            'description' => $fabricant->description,
            'status' => $fabricant->status,
            'department_code' => $fabricant->department->code,
            'department_name' => $fabricant->department->name,
            'votes_count' => $fabricant->votes_count ?? 0,
            'click_count' => $fabricant->click_count ?? 0,
            'slug' => $fabricant->slug,
            'email' => $fabricant->email,
            'creation_date' => $fabricant->creation_date,
            'updated_at' => $fabricant->updated_at
        ];
    }
}
