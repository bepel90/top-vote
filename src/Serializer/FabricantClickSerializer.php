<?php

namespace Bepel90\TopVote\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;

class FabricantClickSerializer extends AbstractSerializer
{
    protected $type = 'fabricant-clicks';

    protected function getDefaultAttributes($fabricantClick)
    {
        return [
            'id' => $fabricantClick->id,
            'fabricant_id' => $fabricantClick->fabricant_id,
            'ip_address' => $fabricantClick->ip_address,
            'created_at' => $this->formatDate($fabricantClick->created_at),
        ];
    }
}
