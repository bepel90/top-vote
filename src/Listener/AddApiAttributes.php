<?php

namespace Bepel90\TopVote\Listener;

use Illuminate\Contracts\Events\Dispatcher;
use Flarum\Api\Event\Serializing;
use Flarum\User\User;

class AddApiAttributes
{
    public function subscribe(Dispatcher $events)
    {
        $events->listen(Serializing::class, [$this, 'addAttributes']);
    }

    public function addAttributes(Serializing $event)
    {
        if ($event->isSerializer(User::class)) {
            $event->attributes['canVote'] = $event->actor->can('vote', $event->model);
        }
    }
}
