<?php

namespace Bepel90\TopVote\Middleware;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Arr;
use Bepel90\TopVote\Models\FabricantClick;

class LogFabricantClickMiddleware implements MiddlewareInterface
{
    public function process(Request $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $slug = Arr::get($request->getQueryParams(), 'id'); // Get the slug from the URL

        // Extract ID from the slug
        $id = explode('-', $slug)[0];

        // Get IP address from the request
        $ipAddress = $request->getAttribute('ip_address') ?? $request->getServerParams()['REMOTE_ADDR'] ?? null;

        if ($ipAddress && $id) {
            // Log the click
            FabricantClick::create([
                'fabricant_id' => $id,
                'ip_address' => $ipAddress,
            ]);
        }

        return $handler->handle($request);
    }
}
