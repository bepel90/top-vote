<?php

namespace Bepel90\TopVote\Api\Controller;

use Flarum\Api\Controller\AbstractCreateController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Bepel90\TopVote\Models\FabricantClick;
use Illuminate\Support\Arr;
use Flarum\User\Exception\PermissionDeniedException;

class LogFabricantClickController extends AbstractCreateController
{
    protected $serializer = null; // Pas besoin de sérialiseur pour cette opération

    protected function data(Request $request, Document $document)
    {
        $actor = $request->getAttribute('actor');
        if (!$actor->isAuthenticated()) {
            throw new PermissionDeniedException();
        }

        $fabricantId = Arr::get($request->getQueryParams(), 'id');
        $ipAddress = $request->getServerParams()['REMOTE_ADDR'];

        $click = new FabricantClick();
        $click->fabricant_id = $fabricantId;
        $click->ip_address = $ipAddress;
        $click->save();

        return $click;
    }
}
