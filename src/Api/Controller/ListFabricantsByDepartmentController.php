<?php

namespace Bepel90\TopVote\Api\Controller;

use Flarum\Api\Controller\AbstractListController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Bepel90\TopVote\Models\Fabricant;
use Bepel90\TopVote\Models\Department;
use Bepel90\TopVote\Serializer\FabricantSerializer;
use Illuminate\Support\Arr;

class ListFabricantsByDepartmentController extends AbstractListController
{
    public $serializer = FabricantSerializer::class;

    protected function data(Request $request, Document $document)
    {
        $departmentCode = Arr::get($request->getQueryParams(), 'id');

        // Find the department by its code
        $department = Department::where('code', $departmentCode)->first();

        if (!$department) {
            // Handle the case where the department is not found
            return [];
        }

        // Get fabricants for the found department and include the vote count
        return Fabricant::withCount('votes')->where('department_id', $department->id)->get();
    }
}
