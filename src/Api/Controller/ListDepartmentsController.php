<?php

namespace Bepel90\TopVote\Api\Controller;

use Flarum\Api\Controller\AbstractListController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Bepel90\TopVote\Models\Department;
use Bepel90\TopVote\Serializer\DepartmentSerializer;

class ListDepartmentsController extends AbstractListController
{
    public $serializer = DepartmentSerializer::class;

    protected function data(Request $request, Document $document)
    {
        return Department::all();
    }
}
