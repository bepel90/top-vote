<?php

namespace Bepel90\TopVote\Api\Controller;

use Flarum\Api\Controller\AbstractListController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Bepel90\TopVote\Models\Fabricant;
use Bepel90\TopVote\Serializer\FabricantSerializer;

class ListFabricantsController extends AbstractListController
{
    public $serializer = FabricantSerializer::class;

    protected function data(Request $request, Document $document)
    {
        return Fabricant::with('clicks', 'votes')->get();
    }
}
