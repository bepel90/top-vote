<?php

namespace Bepel90\TopVote\Api\Controller;

use Flarum\Api\Controller\AbstractShowController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Bepel90\TopVote\Models\Fabricant;
use Bepel90\TopVote\Serializer\FabricantSerializer;
use Illuminate\Support\Arr;
use Bepel90\TopVote\Models\FabricantClick;

class ShowFabricantController extends AbstractShowController
{
    public $serializer = FabricantSerializer::class;

    protected function data(Request $request, Document $document)
    {
        $slug = Arr::get($request->getQueryParams(), 'slug');

        // Extract ID from the slug using regex
        if (preg_match('/^(\d+)-/', $slug, $matches)) {
            $id = $matches[1];
        } else {
            // Fallback to another method if slug does not match the expected pattern
            // Assuming `id` might be passed directly if not matching the slug pattern
            $id = is_numeric($slug) ? (int) $slug : null;
            
            if (!$id) {
                throw new \Exception("Invalid slug format.");
            }
        }

        return Fabricant::findOrFail($id);
    }
}