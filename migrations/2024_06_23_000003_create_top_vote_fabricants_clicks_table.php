<?php

use Flarum\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

return Migration::createTable('top_vote_fabricants_clicks', function (Blueprint $table) {
    $table->increments('id');
    $table->unsignedInteger('fabricant_id');
    $table->string('ip_address');
    $table->timestamps(); // This will create `created_at` and `updated_at` columns

    $table->foreign('fabricant_id')->references('id')->on('top_vote_fabricants')->onDelete('cascade');
});

return Migration::dropTable('top_vote_fabricants_clicks');
