<?php

use Flarum\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

return Migration::createTable('top_vote_fabricants', function (Blueprint $table) {
    $table->increments('id');
    $table->string('commercial_name');
    $table->string('company_name');
    $table->string('siret')->unique();
    $table->string('website')->nullable();
    $table->string('image')->nullable();
    $table->date('creation_date')->nullable();
    $table->string('address')->nullable();
    $table->string('phone')->nullable();
    $table->text('description')->nullable();
    $table->enum('status', ['verified', 'unverified', 'suspended'])->default('unverified');
    $table->unsignedInteger('department_id')->nullable();
    $table->timestamp('added_date')->useCurrent();

    $table->timestamps();

    $table->foreign('department_id')->references('id')->on('top_vote_departments')->onDelete('set null');
});
