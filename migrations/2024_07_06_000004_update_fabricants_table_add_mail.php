<?php

use Flarum\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

return Migration::addColumns('top_vote_fabricants', [
    'email' => ['string', 'length' => 255, 'nullable' => true]
]);