<?php

use Flarum\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

return Migration::createTable('top_vote_departments', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->string('code');
    $table->timestamps();
});