// js/src/forum/index.js
import { extend, override } from 'flarum/common/extend';
import app from 'flarum/forum/app';
import FabricantListPage from './components/FabricantListPage';
import FabricantShowPage from './components/FabricantShowPage';
import Card from './components/Card';
import ContactCard from './components/ContactCard';

app.initializers.add('top-vote', () => {
  app.routes['fabricants.list'] = {
    path: '/fabricants',
    component: FabricantListPage,
  };
  app.routes['fabricants.show'] = {
    path: '/fabricants/:slug',
    component: FabricantShowPage,
  };
});