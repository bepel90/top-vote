import Component from 'flarum/common/Component';

export default class Card extends Component {
  view(vnode) {
    const { fabricant } = vnode.attrs;
    const { 
      commercial_name, company_name, siret, 
      slug, status, department_name, department_code, votes_count, 
      click_count, image, website, address, phone_number
    } = fabricant.attributes;

    // Préfixe https:// si website n'est pas null ou vide et ne commence pas par http:// ou https://
    let displayWebsite = website && !/^https?:\/\//i.test(website) ? `https://${website}` : website || 'Site web non disponible';

    // Supprimer www. si présent dans l'URL
    if (displayWebsite.includes('www.')) {
      displayWebsite = displayWebsite.replace('www.', '');
    }

    // Formater le numéro de téléphone
    const formatPhoneNumber = (number) => {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Numéro de téléphone non disponible';
    };

    return m("div", { class: "card", onclick: () => window.location.href = `/fabricants/${slug}` },
      [
        m("div", { class: "card-image" },
          [
            m("img", { src: image || 'default-image.jpg', alt: commercial_name || 'Image non disponible' }),
            //m("div", { class: "card-heart" },
            //  m("i", { class: "far fa-heart" })
            //)
          ]
        ),
        m("div", { class: "card-content" },
          [
            m("h2", commercial_name || 'Nom commercial non disponible'),
            //m("p",
            //  [
            //    m("i", { class: "fa fa-eye" }),
            //    ` ${click_count || 0}`
            //  ]
            //),
            m("p",
              [
                m("i", { class: "fa fa-phone fa-rotate-90" }),
                ` ${formatPhoneNumber(phone_number)}`
              ]
            ),
            m("p",
              [
                m("i", { class: "fa fa-map-marker" }),
                ` ${address || 'Adresse non disponible'}`
              ]
            ),
            m("p",
              [
                m("i", { class: "fa fa-link" }),
                m("a", { class: "site-link", href: displayWebsite, rel: "noopener noreferrer nofollow" }, ` ${displayWebsite}`)
              ]
            ),
            m("div", { class: "card-bottom" },
              [
                m("span",
                  [
                    m("i", { class: "fa fa-globe" }),
                    ` ${department_name ? `${department_name} (${department_code})` : 'Département non disponible'}`
                  ]
                ),
                m("span",
                  [
                    m("i", { class: "far fa-eye" }),
                    ` ${click_count || 0}`
                  ]
                )
              ]
            )
          ]
        )
      ]
    );
  }
}
