import Page from 'flarum/common/components/Page';
import app from 'flarum/forum/app';
import ContactCard from './ContactCard';
import Faq from './Faq';

export default class FabricantShowPage extends Page {
  oninit(vnode) {
    super.oninit(vnode);
    this.slug = m.route.param('slug');
    this.fabricant = null;
    this.isLoading = true;
    this.hasError = false;

    app.request({
      method: 'GET',
      url: `${app.forum.attribute('apiUrl')}/fabricants/${this.slug}`,
    }).then(result => {
      this.fabricant = result.data;
      this.isLoading = false;
      m.redraw();
    }).catch(error => {
      this.hasError = true;
      this.isLoading = false;
      m.redraw();
    });
  }

  view() {
    if (this.isLoading) {
      return m("div", { class: "loading" }, "Loading...");
    }

    if (this.hasError) {
      return m("div", { class: "error" }, "An error occurred while loading the fabricant data.");
    }

    const fabricant = this.fabricant;
    const {
      commercial_name, image, address, phone_number, website, email, description
    } = fabricant.attributes;

    return m("div", [
      m("header", { class: "Hero WelcomeHero" },
        m("div", { class: "container" },
          m("button", {
            class: "Hero-close Button Button--icon Button--link hasIcon",
            type: "button",
            "aria-label": "Masquer le message de bienvenue"
          },
            m("i", {
              "aria-hidden": "true",
              class: "icon fas fa-times Button-icon"
            }),
            m("span", { class: "Button-label" })
          ),
          m("div", { class: "containerNarrow" },
            m("h1", { class: "Hero-title" }, "Trouvez votre fabricant de Tiny House"),
            m("div", { class: "Hero-subtitle" }, "Explorez notre annuaire des fabricants de Tiny House, mis à jour régulièrement. Trouvez le constructeur idéal pour votre projet !")
          )
        )
      ),
      m("div", { class: "container" },
        m(ContactCard, { fabricant }),
        m(Faq, { fabricant })
      )
    ]);
  }
}
