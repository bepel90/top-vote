import Page from 'flarum/common/components/Page';
import app from 'flarum/forum/app';
import Component from 'flarum/common/Component';
import Card from './Card';
import FilterBar from './FilterBar'


export default class FabricantListPage extends Page {
    oninit(vnode) {
        super.oninit(vnode);
        app.setTitle("Annuaire des fabricants de Tiny House");
        this.fabricants = [];
        this.isLoading = true;
        app.request({
            method: 'GET',
            url: app.forum.attribute('apiUrl') + '/fabricants',
        }).then(result => {
            this.fabricants = result.data; // Stocker les résultats dans fabricants
            this.isLoading = false; // Mettre à jour l'état de chargement
            m.redraw(); // Redessiner la vue
        }).catch(error => {
            this.isLoading = false; // Mettre à jour l'état de chargement en cas d'erreur
            m.redraw(); // Redessiner la vue
        });
    }
    view() {
      // Générer les éléments Card dans une grille de 3 colonnes par ligne
      const cards = this.fabricants.map(fabricant => {
        return m(Card, { key: fabricant.id, fabricant });
      });
  
    // Encapsuler les éléments dans un conteneur avec classe CSS pour la grille
    return m("div", { class: "custom-area" },
      [
        m("header", { class: "Hero WelcomeHero" },
        m("div", { class: "container" },
          m("button", { 
            class: "Hero-close Button Button--icon Button--link hasIcon", 
            type: "button", 
            "aria-label": "Masquer le message de bienvenue" 
          },
            m("i", { 
              "aria-hidden": "true", 
              class: "icon fas fa-times Button-icon" 
            }),
            m("span", { class: "Button-label" })
          ),
          m("div", { class: "containerNarrow" },
            m("h1", { class: "Hero-title" }, "Trouvez votre fabricant de Tiny House"),
            m("div", { class: "Hero-subtitle" }, "Explorez notre annuaire des fabricants de Tiny House, mis à jour régulièrement. Trouvez le constructeur idéal pour votre projet !")
          )
        )
      ),         
        m("div", { class: "container" }, 
          m("div", { class: "cards-container" }, cards)
        )
      ]
    );
  }
}