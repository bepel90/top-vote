import Component from 'flarum/common/Component';

export default class ContactCard extends Component {
  view(vnode) {
    const { fabricant } = vnode.attrs;
    const { 
      address, phone_number, website, email, image, commercial_name, siret, creation_date, department_name, department_code, status, updated_at, description
    } = fabricant.attributes;

    // Préfixe https:// si website n'est pas null ou vide et ne commence pas par http:// ou https://
    let displayWebsite = website && !/^https?:\/\//i.test(website) ? `https://${website}` : website || 'Site web non disponible';

    // Supprimer www. si présent dans l'URL
    if (displayWebsite.includes('www.')) {
      displayWebsite = displayWebsite.replace('www.', '');
    }

    // Formater le numéro de téléphone
    const formatPhoneNumber = (number) => {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Téléphone non disponible';
    };

    function calculateDuration(creationDate) {
      const now = new Date();
      const createdDate = new Date(creationDate);
    
      const diffInMonths = (now.getFullYear() - createdDate.getFullYear()) * 12 + (now.getMonth() - createdDate.getMonth());
    
      if (diffInMonths >= 12) {
        const years = Math.floor(diffInMonths / 12);
        return `${years} ${years > 1 ? 'ans' : 'an'}`;
      } else {
        return `${diffInMonths} ${diffInMonths > 1 ? 'mois' : 'mois'}`;
      }
    }

    function formatDateToFrench(dateString) {
      const date = new Date(dateString);
      const day = String(date.getDate()).padStart(2, '0');
      const month = String(date.getMonth() + 1).padStart(2, '0');
      const year = date.getFullYear();
      return `${day}/${month}/${year}`;
    }

    function getBadge(status) {
      switch (status) {
        case 'verified':
          return m("div", { class: "custom-info-item badge-primary" }, 
            m("i", { class: "fa fa-shield-alt" }),
            m("span", "Informations vérifiées")
          );
        case 'unverified':
          return m("div", { class: "custom-info-item badge-warning" }, 
            m("i", { class: "fa fa-question-circle" }),
            m("span", "Non vérifié")
          );
        case 'suspended':
          return m("div", { class: "custom-info-item badge-danger" }, 
            m("i", { class: "fa fa-ban" }),
            m("span", "Suspendu")
          );
        default:
          return m("div", { class: "custom-info-item badge-secondary" }, 
            m("i", { class: "fa fa-clock" }),
            m("span", "Vérification en cours")
          );
      }
    }

    return m("div", { class: "contact-card" }, 
      m("div", { class: "custom-space-y-4" }, [
        m("div", [
          m("div", { class: "manufacturer-name-container" }, 
            m("h1", { class: "manufacturer-name" }, 
              commercial_name
            )
          ),
          m("div", { class: "custom-info-container" }, [
            m("div", { class: "custom-info-item badge-primary" }, 
              m("i", { class: "fa fa-globe" }),
              m("span", 
                ` ${department_name ? `${department_name} (${department_code})` : 'Département non disponible'}`
              )
            ),
            m("div", { class: "custom-info-item badge-primary" }, [
              m("i", { class: "fa fa-birthday-cake" }),
              m("span", calculateDuration(creation_date))
            ]),
            getBadge(status) // Utilisation de la fonction pour afficher le badge correct
          ])
        ]),
        m("div", { class: "custom-tabs" }, [
          m("button", { class: "custom-button-outline active" }, "Coordonnées"),
          m("button", { 
            class: "custom-button-outline", 
            onclick: () => {
              document.getElementById("faq-section").scrollIntoView({ behavior: 'smooth' });
            }
          }, "Questions fréquentes"),
        ]),
        m("div", { class: "custom-grid" }, [
          m("div", { class: "custom-card-inner" }, [
            m("div", { class: "custom-info" }, [
              m("i", { class: "fa fa-phone-alt" }),
              m("span", { class: "text-muted" }, formatPhoneNumber(phone_number)),
              phone_number && m("a", { href: `tel:${phone_number}`, class: "link-button" }, [
                m("i", { class: "fa fa-phone-volume" }),
                m("span", " Appeler")
              ])
            ]),
            m("div", { class: "custom-info mt-2" }, [
              m("i", { class: "fa fa-envelope" }),
              m("span", { class: "text-muted" }, email || "Email non disponible"),
              email && m("a", { href: `mailto:${email}`, class: "link-button" }, [
                m("i", { class: "fa fa-paper-plane mr-1" }),
                m("span", "Écrire")
              ])
            ]),
            m("div", { class: "custom-info mt-2" }, [
              m("i", { class: "fa fa-map-marker" }),
              m("span", { class: "text-muted" }, address || "Adresse non disponible")
            ]),
            m("div", { class: "custom-info mt-2" }, [
              m("i", { class: "fa fa-link" }),
              m("span", { class: "text-muted" }, displayWebsite),
              website && m("a", { href: displayWebsite, class: "link-button", target: "_blank", rel: "nofollow" }, [
                m("i", { class: "fa fa-arrow-right mr-1" }),
                m("span", "Ouvrir")
              ])
            ]),
            m("div", { class: "custom-info mt-2" }, [
              m("i", { class: "fa fa-calendar" }),
              m("span", `Créé le ${formatDateToFrench(creation_date)}`),
            ]),
            m("div", { class: "custom-info mt-2" }, [
              m("i", { class: "fa fa-info-circle" }),
              m("span", "SIRET"),
              m("span", { class: "text-muted" }, siret)
            ])
          ]),
          m("div", { class: "custom-card-inner custom-card-center" }, 
            m("button", {
              class: "Button Button--primary",
              onclick: () => {
                if (!address) return;
                const formattedAddress = encodeURIComponent(address.replace(/ /g, '-'));
                const url = "https://www.google.com/maps?q=" + formattedAddress;
                window.open(url, '_blank');
              }
            }, [
              m("i", { class: address ? "fa fa-map-marker-alt" : "fa fa-exclamation-circle" }),
              address ? " Voir sur la carte" : " Adresse manquante"
            ])
          )
        ]),
        m("div", { id: "faq-section" }, [
          // Contenu de la section FAQ ici
        ])
      ])
    );
  }
}
