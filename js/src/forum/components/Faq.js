import Component from 'flarum/common/Component';

export default class FAQComponent extends Component {
  oninit(vnode) {
    super.oninit(vnode);
    this.expandedIndex = null; // Keeps track of the currently expanded item
    this.selectedTheme = "general"; // Default theme
  }

  toggle(index) {
    this.expandedIndex = this.expandedIndex === index ? null : index;
  }

  setSelectedTheme(theme) {
    this.selectedTheme = theme;
  }

  view(vnode) {
    const { fabricant } = vnode.attrs;
    const {
      commercial_name,
      address,
      phone_number,
      website,
      email,
      description,
      creation_date,
      department_name,
      department_code,
      status,
      updated_at
    } = fabricant.attributes;

    // Helper functions
    const formatPhoneNumber = (number) => {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Numéro de téléphone non disponible';
    };

    const calculateDuration = (creationDate) => {
      const now = new Date();
      const createdDate = new Date(creationDate);
      const diffInMonths = (now.getFullYear() - createdDate.getFullYear()) * 12 + (now.getMonth() - createdDate.getMonth());

      if (diffInMonths >= 12) {
        const years = Math.floor(diffInMonths / 12);
        return `${years} ${years > 1 ? 'ans' : 'an'}`;
      } else {
        return `${diffInMonths} mois`;
      }
    };

    const formatDateToFrench = (dateString) => {
      const date = new Date(dateString);
      const day = String(date.getDate()).padStart(2, '0');
      const month = String(date.getMonth() + 1).padStart(2, '0');
      const year = date.getFullYear();
      return `${day}/${month}/${year}`;
    };

    // Define FAQ items
    const faqs = [
        { theme: "general", question: `Quel est le prix d'une tiny house fabriquée par ${commercial_name} ?`, answer: `Le prix d'une tiny house de ${commercial_name} varie en fonction du modèle et des options choisies. Contactez-nous pour obtenir un devis personnalisé.` },
        { theme: "general", question: `Comment financer une tiny house de ${commercial_name} ?`, answer: `Il existe plusieurs options pour financer l'achat de votre tiny house de ${commercial_name}. Vous pouvez opter pour un prêt personnel, ou encore le financement participatif.` },
        { theme: "general", question: `Quels services propose ${commercial_name} ?`, answer: `${commercial_name} propose une gamme complète de services incluant la conception personnalisée, la fabrication sur mesure, l'installation et la livraison, le service après-vente, et des conseils en aménagement.` },
        { theme: "general", question: `Pourquoi choisir une tiny house ${commercial_name} ?`, answer: `Les tiny houses de ${commercial_name} sont réputées pour leur qualité supérieure, leur durabilité et leur design innovant.` },
        { theme: "delivery", question: `J'habite un autre département, est-ce que ${commercial_name} peut me livrer ?`, answer: `${commercial_name} est basé dans le ${department_name} (${department_code}), mais nous pouvons livrer votre tiny house partout en France ! Contactez-nous pour établir un devis personnalisé et organiser la livraison de votre future maison.` },
        { theme: "delivery", question: `Où peut-on visiter une tiny house de ${commercial_name} près de chez moi ?`, answer: `Vous pouvez visiter une tiny house ${commercial_name} dans notre atelier de fabrication. Contactez-nous pour prendre rendez-vous. Vous pouvez également essayer une de nos tiny houses en la louant via la page 'Séjour en tiny house'.` },
        { theme: "delivery", question: `Quelle est l'adresse et le département de ${commercial_name} ?`, answer: `${address} - ${department_name} (${department_code})` },
        { theme: "quality", question: `Quels matériaux utilise ${commercial_name} pour ses tiny houses ?`, answer: `${commercial_name} utilise des matériaux écologiques et durables pour la construction de ses tiny houses, garantissant ainsi une faible empreinte carbone et une grande longévité.` },
        { theme: "quality", question: `Quelle est la durée de vie d'une tiny house construite par ${commercial_name} ?`, answer: `Les tiny houses de ${commercial_name} sont conçues pour durer de nombreuses années grâce à l'utilisation de matériaux de haute qualité et à une construction robuste.` },
        { theme: "quality", question: `Quels sont les avantages d'une tiny house ${commercial_name} ?`, answer: `Les tiny houses offrent de nombreux avantages : moins chères à financer et entretenir qu'une maison traditionnelle, elles offrent les mêmes avantages en termes d'intimité et d'espace personnel. Elles nécessitent moins d'entretien et peuvent générer des revenus en étant louées.` },
        { theme: "models", question: `Quels modèles de tiny houses propose ${commercial_name} ?`, answer: `${commercial_name} offre une gamme variée de modèles de tiny houses, des designs compacts et fonctionnels aux modèles plus spacieux et luxueux.` },
        { theme: "models", question: `Comment personnaliser une tiny house de ${commercial_name} selon ses besoins ?`, answer: `${commercial_name} offre de nombreuses options de personnalisation pour répondre aux besoins spécifiques de chaque client, des plans de sol aux finitions intérieures.` }
    ];

    // Filter FAQs based on selected theme
    const filteredFaqs = faqs.filter(faq => faq.theme === this.selectedTheme);

    return m("div", { class: "faq-container" }, 
      m("div", { class: "faq-header" }, [
        m("h2", { class: "faq-title" }, "Questions fréquentes"),
        m("div", { class: "filter-select-container" }, [
          m("select", {
            class: "filter-select",
            value: this.selectedTheme,
            onchange: (e) => this.setSelectedTheme(e.target.value)
          }, [
            m("option", { value: "general" }, "Général"),
            m("option", { value: "delivery" }, "Livraison"),
            m("option", { value: "quality" }, "Fabrication"),
            m("option", { value: "models" }, "Modèles")
          ]),
          m("i", { class: "fa fa-chevron-down filter-icon" })
        ])
      ]),
      m("div", { class: "faq-items" }, 
        filteredFaqs.map((faq, index) => {
          const isExpanded = this.expandedIndex === index;
          return m("div", { class: `faq-item ${isExpanded ? 'expanded' : ''}` }, [
            m("div", {
              class: "faq-question",
              onclick: () => this.toggle(index)
            }, [
              m("span", faq.question),
              m("i", { class: `fa fa-chevron-${isExpanded ? 'up' : 'down'}` }),
            ]),
            isExpanded && m("p", { class: "faq-answer" }, faq.answer)
          ]);
        })
      )
    );
  }
}
