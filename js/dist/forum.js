/******/ (() => { // webpackBootstrap
/******/ 	// runtime can't be in strict mode because a global variable is assign and maybe created.
/******/ 	var __webpack_modules__ = ({

/***/ "./src/forum/components/Card.js":
/*!**************************************!*\
  !*** ./src/forum/components/Card.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Card)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/Component */ "flarum/common/Component");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__);


var Card = /*#__PURE__*/function (_Component) {
  function Card() {
    return _Component.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(Card, _Component);
  var _proto = Card.prototype;
  _proto.view = function view(vnode) {
    var fabricant = vnode.attrs.fabricant;
    var _fabricant$attributes = fabricant.attributes,
      commercial_name = _fabricant$attributes.commercial_name,
      company_name = _fabricant$attributes.company_name,
      siret = _fabricant$attributes.siret,
      slug = _fabricant$attributes.slug,
      status = _fabricant$attributes.status,
      department_name = _fabricant$attributes.department_name,
      department_code = _fabricant$attributes.department_code,
      votes_count = _fabricant$attributes.votes_count,
      click_count = _fabricant$attributes.click_count,
      image = _fabricant$attributes.image,
      website = _fabricant$attributes.website,
      address = _fabricant$attributes.address,
      phone_number = _fabricant$attributes.phone_number;

    // Préfixe https:// si website n'est pas null ou vide et ne commence pas par http:// ou https://
    var displayWebsite = website && !/^https?:\/\//i.test(website) ? "https://" + website : website || 'Site web non disponible';

    // Supprimer www. si présent dans l'URL
    if (displayWebsite.includes('www.')) {
      displayWebsite = displayWebsite.replace('www.', '');
    }

    // Formater le numéro de téléphone
    var formatPhoneNumber = function formatPhoneNumber(number) {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Numéro de téléphone non disponible';
    };
    return m("div", {
      "class": "card",
      onclick: function onclick() {
        return window.location.href = "/fabricants/" + slug;
      }
    }, [m("div", {
      "class": "card-image"
    }, [m("img", {
      src: image || 'default-image.jpg',
      alt: commercial_name || 'Image non disponible'
    })
    //m("div", { class: "card-heart" },
    //  m("i", { class: "far fa-heart" })
    //)
    ]), m("div", {
      "class": "card-content"
    }, [m("h2", commercial_name || 'Nom commercial non disponible'),
    //m("p",
    //  [
    //    m("i", { class: "fa fa-eye" }),
    //    ` ${click_count || 0}`
    //  ]
    //),
    m("p", [m("i", {
      "class": "fa fa-phone fa-rotate-90"
    }), " " + formatPhoneNumber(phone_number)]), m("p", [m("i", {
      "class": "fa fa-map-marker"
    }), " " + (address || 'Adresse non disponible')]), m("p", [m("i", {
      "class": "fa fa-link"
    }), m("a", {
      "class": "site-link",
      href: displayWebsite,
      rel: "noopener noreferrer nofollow"
    }, " " + displayWebsite)]), m("div", {
      "class": "card-bottom"
    }, [m("span", [m("i", {
      "class": "fa fa-globe"
    }), " " + (department_name ? department_name + " (" + department_code + ")" : 'Département non disponible')]), m("span", [m("i", {
      "class": "far fa-eye"
    }), " " + (click_count || 0)])])])]);
  };
  return Card;
}((flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/components/ContactCard.js":
/*!*********************************************!*\
  !*** ./src/forum/components/ContactCard.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ ContactCard)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/Component */ "flarum/common/Component");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__);


var ContactCard = /*#__PURE__*/function (_Component) {
  function ContactCard() {
    return _Component.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(ContactCard, _Component);
  var _proto = ContactCard.prototype;
  _proto.view = function view(vnode) {
    var fabricant = vnode.attrs.fabricant;
    var _fabricant$attributes = fabricant.attributes,
      address = _fabricant$attributes.address,
      phone_number = _fabricant$attributes.phone_number,
      website = _fabricant$attributes.website,
      email = _fabricant$attributes.email,
      image = _fabricant$attributes.image,
      commercial_name = _fabricant$attributes.commercial_name,
      siret = _fabricant$attributes.siret,
      creation_date = _fabricant$attributes.creation_date,
      department_name = _fabricant$attributes.department_name,
      department_code = _fabricant$attributes.department_code,
      status = _fabricant$attributes.status,
      updated_at = _fabricant$attributes.updated_at,
      description = _fabricant$attributes.description;

    // Préfixe https:// si website n'est pas null ou vide et ne commence pas par http:// ou https://
    var displayWebsite = website && !/^https?:\/\//i.test(website) ? "https://" + website : website || 'Site web non disponible';

    // Supprimer www. si présent dans l'URL
    if (displayWebsite.includes('www.')) {
      displayWebsite = displayWebsite.replace('www.', '');
    }

    // Formater le numéro de téléphone
    var formatPhoneNumber = function formatPhoneNumber(number) {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Téléphone non disponible';
    };
    function calculateDuration(creationDate) {
      var now = new Date();
      var createdDate = new Date(creationDate);
      var diffInMonths = (now.getFullYear() - createdDate.getFullYear()) * 12 + (now.getMonth() - createdDate.getMonth());
      if (diffInMonths >= 12) {
        var years = Math.floor(diffInMonths / 12);
        return years + " " + (years > 1 ? 'ans' : 'an');
      } else {
        return diffInMonths + " " + (diffInMonths > 1 ? 'mois' : 'mois');
      }
    }
    function formatDateToFrench(dateString) {
      var date = new Date(dateString);
      var day = String(date.getDate()).padStart(2, '0');
      var month = String(date.getMonth() + 1).padStart(2, '0');
      var year = date.getFullYear();
      return day + "/" + month + "/" + year;
    }
    function getBadge(status) {
      switch (status) {
        case 'verified':
          return m("div", {
            "class": "custom-info-item badge-primary"
          }, m("i", {
            "class": "fa fa-shield-alt"
          }), m("span", "Informations vérifiées"));
        case 'unverified':
          return m("div", {
            "class": "custom-info-item badge-warning"
          }, m("i", {
            "class": "fa fa-question-circle"
          }), m("span", "Non vérifié"));
        case 'suspended':
          return m("div", {
            "class": "custom-info-item badge-danger"
          }, m("i", {
            "class": "fa fa-ban"
          }), m("span", "Suspendu"));
        default:
          return m("div", {
            "class": "custom-info-item badge-secondary"
          }, m("i", {
            "class": "fa fa-clock"
          }), m("span", "Vérification en cours"));
      }
    }
    return m("div", {
      "class": "contact-card"
    }, m("div", {
      "class": "custom-space-y-4"
    }, [m("div", [m("div", {
      "class": "manufacturer-name-container"
    }, m("h1", {
      "class": "manufacturer-name"
    }, commercial_name)), m("div", {
      "class": "custom-info-container"
    }, [m("div", {
      "class": "custom-info-item badge-primary"
    }, m("i", {
      "class": "fa fa-globe"
    }), m("span", " " + (department_name ? department_name + " (" + department_code + ")" : 'Département non disponible'))), m("div", {
      "class": "custom-info-item badge-primary"
    }, [m("i", {
      "class": "fa fa-birthday-cake"
    }), m("span", calculateDuration(creation_date))]), getBadge(status) // Utilisation de la fonction pour afficher le badge correct
    ])]), m("div", {
      "class": "custom-tabs"
    }, [m("button", {
      "class": "custom-button-outline active"
    }, "Coordonnées"), m("button", {
      "class": "custom-button-outline",
      onclick: function onclick() {
        document.getElementById("faq-section").scrollIntoView({
          behavior: 'smooth'
        });
      }
    }, "Questions fréquentes")]), m("div", {
      "class": "custom-grid"
    }, [m("div", {
      "class": "custom-card-inner"
    }, [m("div", {
      "class": "custom-info"
    }, [m("i", {
      "class": "fa fa-phone-alt"
    }), m("span", {
      "class": "text-muted"
    }, formatPhoneNumber(phone_number)), phone_number && m("a", {
      href: "tel:" + phone_number,
      "class": "link-button"
    }, [m("i", {
      "class": "fa fa-phone-volume"
    }), m("span", " Appeler")])]), m("div", {
      "class": "custom-info mt-2"
    }, [m("i", {
      "class": "fa fa-envelope"
    }), m("span", {
      "class": "text-muted"
    }, email || "Email non disponible"), email && m("a", {
      href: "mailto:" + email,
      "class": "link-button"
    }, [m("i", {
      "class": "fa fa-paper-plane mr-1"
    }), m("span", "Écrire")])]), m("div", {
      "class": "custom-info mt-2"
    }, [m("i", {
      "class": "fa fa-map-marker"
    }), m("span", {
      "class": "text-muted"
    }, address || "Adresse non disponible")]), m("div", {
      "class": "custom-info mt-2"
    }, [m("i", {
      "class": "fa fa-link"
    }), m("span", {
      "class": "text-muted"
    }, displayWebsite), website && m("a", {
      href: displayWebsite,
      "class": "link-button",
      target: "_blank",
      rel: "nofollow"
    }, [m("i", {
      "class": "fa fa-arrow-right mr-1"
    }), m("span", "Ouvrir")])]), m("div", {
      "class": "custom-info mt-2"
    }, [m("i", {
      "class": "fa fa-calendar"
    }), m("span", "Cr\xE9\xE9 le " + formatDateToFrench(creation_date))]), m("div", {
      "class": "custom-info mt-2"
    }, [m("i", {
      "class": "fa fa-info-circle"
    }), m("span", "SIRET"), m("span", {
      "class": "text-muted"
    }, siret)])]), m("div", {
      "class": "custom-card-inner custom-card-center"
    }, m("button", {
      "class": "Button Button--primary",
      onclick: function onclick() {
        if (!address) return;
        var formattedAddress = encodeURIComponent(address.replace(/ /g, '-'));
        var url = "https://www.google.com/maps?q=" + formattedAddress;
        window.open(url, '_blank');
      }
    }, [m("i", {
      "class": address ? "fa fa-map-marker-alt" : "fa fa-exclamation-circle"
    }), address ? " Voir sur la carte" : " Adresse manquante"]))]), m("div", {
      id: "faq-section"
    }, [
      // Contenu de la section FAQ ici
    ])]));
  };
  return ContactCard;
}((flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/components/FabricantListPage.js":
/*!***************************************************!*\
  !*** ./src/forum/components/FabricantListPage.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ FabricantListPage)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/components/Page */ "flarum/common/components/Page");
/* harmony import */ var flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/forum/app */ "flarum/forum/app");
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_forum_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/common/Component */ "flarum/common/Component");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_common_Component__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Card */ "./src/forum/components/Card.js");
/* harmony import */ var _FilterBar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./FilterBar */ "./src/forum/components/FilterBar.js");






var FabricantListPage = /*#__PURE__*/function (_Page) {
  function FabricantListPage() {
    return _Page.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(FabricantListPage, _Page);
  var _proto = FabricantListPage.prototype;
  _proto.oninit = function oninit(vnode) {
    var _this = this;
    _Page.prototype.oninit.call(this, vnode);
    flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default().setTitle("Annuaire des fabricants de Tiny House");
    this.fabricants = [];
    this.isLoading = true;
    flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default().request({
      method: 'GET',
      url: flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default().forum.attribute('apiUrl') + '/fabricants'
    }).then(function (result) {
      _this.fabricants = result.data; // Stocker les résultats dans fabricants
      _this.isLoading = false; // Mettre à jour l'état de chargement
      m.redraw(); // Redessiner la vue
    })["catch"](function (error) {
      _this.isLoading = false; // Mettre à jour l'état de chargement en cas d'erreur
      m.redraw(); // Redessiner la vue
    });
  };
  _proto.view = function view() {
    // Générer les éléments Card dans une grille de 3 colonnes par ligne
    var cards = this.fabricants.map(function (fabricant) {
      return m(_Card__WEBPACK_IMPORTED_MODULE_4__["default"], {
        key: fabricant.id,
        fabricant: fabricant
      });
    });

    // Encapsuler les éléments dans un conteneur avec classe CSS pour la grille
    return m("div", {
      "class": "custom-area"
    }, [m("header", {
      "class": "Hero WelcomeHero"
    }, m("div", {
      "class": "container"
    }, m("button", {
      "class": "Hero-close Button Button--icon Button--link hasIcon",
      type: "button",
      "aria-label": "Masquer le message de bienvenue"
    }, m("i", {
      "aria-hidden": "true",
      "class": "icon fas fa-times Button-icon"
    }), m("span", {
      "class": "Button-label"
    })), m("div", {
      "class": "containerNarrow"
    }, m("h1", {
      "class": "Hero-title"
    }, "Trouvez votre fabricant de Tiny House"), m("div", {
      "class": "Hero-subtitle"
    }, "Explorez notre annuaire des fabricants de Tiny House, mis à jour régulièrement. Trouvez le constructeur idéal pour votre projet !")))), m("div", {
      "class": "container"
    }, m("div", {
      "class": "cards-container"
    }, cards))]);
  };
  return FabricantListPage;
}((flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/components/FabricantShowPage.js":
/*!***************************************************!*\
  !*** ./src/forum/components/FabricantShowPage.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ FabricantShowPage)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/components/Page */ "flarum/common/components/Page");
/* harmony import */ var flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/forum/app */ "flarum/forum/app");
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_forum_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ContactCard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContactCard */ "./src/forum/components/ContactCard.js");
/* harmony import */ var _Faq__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Faq */ "./src/forum/components/Faq.js");





var FabricantShowPage = /*#__PURE__*/function (_Page) {
  function FabricantShowPage() {
    return _Page.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(FabricantShowPage, _Page);
  var _proto = FabricantShowPage.prototype;
  _proto.oninit = function oninit(vnode) {
    var _this = this;
    _Page.prototype.oninit.call(this, vnode);
    this.slug = m.route.param('slug');
    this.fabricant = null;
    this.isLoading = true;
    this.hasError = false;
    flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default().request({
      method: 'GET',
      url: flarum_forum_app__WEBPACK_IMPORTED_MODULE_2___default().forum.attribute('apiUrl') + "/fabricants/" + this.slug
    }).then(function (result) {
      _this.fabricant = result.data;
      _this.isLoading = false;
      m.redraw();
    })["catch"](function (error) {
      _this.hasError = true;
      _this.isLoading = false;
      m.redraw();
    });
  };
  _proto.view = function view() {
    if (this.isLoading) {
      return m("div", {
        "class": "loading"
      }, "Loading...");
    }
    if (this.hasError) {
      return m("div", {
        "class": "error"
      }, "An error occurred while loading the fabricant data.");
    }
    var fabricant = this.fabricant;
    var _fabricant$attributes = fabricant.attributes,
      commercial_name = _fabricant$attributes.commercial_name,
      image = _fabricant$attributes.image,
      address = _fabricant$attributes.address,
      phone_number = _fabricant$attributes.phone_number,
      website = _fabricant$attributes.website,
      email = _fabricant$attributes.email,
      description = _fabricant$attributes.description;
    return m("div", [m("header", {
      "class": "Hero WelcomeHero"
    }, m("div", {
      "class": "container"
    }, m("button", {
      "class": "Hero-close Button Button--icon Button--link hasIcon",
      type: "button",
      "aria-label": "Masquer le message de bienvenue"
    }, m("i", {
      "aria-hidden": "true",
      "class": "icon fas fa-times Button-icon"
    }), m("span", {
      "class": "Button-label"
    })), m("div", {
      "class": "containerNarrow"
    }, m("h1", {
      "class": "Hero-title"
    }, "Trouvez votre fabricant de Tiny House"), m("div", {
      "class": "Hero-subtitle"
    }, "Explorez notre annuaire des fabricants de Tiny House, mis à jour régulièrement. Trouvez le constructeur idéal pour votre projet !")))), m("div", {
      "class": "container"
    }, m(_ContactCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
      fabricant: fabricant
    }), m(_Faq__WEBPACK_IMPORTED_MODULE_4__["default"], {
      fabricant: fabricant
    }))]);
  };
  return FabricantShowPage;
}((flarum_common_components_Page__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/components/Faq.js":
/*!*************************************!*\
  !*** ./src/forum/components/Faq.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ FAQComponent)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/Component */ "flarum/common/Component");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__);


var FAQComponent = /*#__PURE__*/function (_Component) {
  function FAQComponent() {
    return _Component.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(FAQComponent, _Component);
  var _proto = FAQComponent.prototype;
  _proto.oninit = function oninit(vnode) {
    _Component.prototype.oninit.call(this, vnode);
    this.expandedIndex = null; // Keeps track of the currently expanded item
    this.selectedTheme = "general"; // Default theme
  };
  _proto.toggle = function toggle(index) {
    this.expandedIndex = this.expandedIndex === index ? null : index;
  };
  _proto.setSelectedTheme = function setSelectedTheme(theme) {
    this.selectedTheme = theme;
  };
  _proto.view = function view(vnode) {
    var _this = this;
    var fabricant = vnode.attrs.fabricant;
    var _fabricant$attributes = fabricant.attributes,
      commercial_name = _fabricant$attributes.commercial_name,
      address = _fabricant$attributes.address,
      phone_number = _fabricant$attributes.phone_number,
      website = _fabricant$attributes.website,
      email = _fabricant$attributes.email,
      description = _fabricant$attributes.description,
      creation_date = _fabricant$attributes.creation_date,
      department_name = _fabricant$attributes.department_name,
      department_code = _fabricant$attributes.department_code,
      status = _fabricant$attributes.status,
      updated_at = _fabricant$attributes.updated_at;

    // Helper functions
    var formatPhoneNumber = function formatPhoneNumber(number) {
      return number ? number.replace(/(\d{2})(?=\d)/g, "$1 ") : 'Numéro de téléphone non disponible';
    };
    var calculateDuration = function calculateDuration(creationDate) {
      var now = new Date();
      var createdDate = new Date(creationDate);
      var diffInMonths = (now.getFullYear() - createdDate.getFullYear()) * 12 + (now.getMonth() - createdDate.getMonth());
      if (diffInMonths >= 12) {
        var years = Math.floor(diffInMonths / 12);
        return years + " " + (years > 1 ? 'ans' : 'an');
      } else {
        return diffInMonths + " mois";
      }
    };
    var formatDateToFrench = function formatDateToFrench(dateString) {
      var date = new Date(dateString);
      var day = String(date.getDate()).padStart(2, '0');
      var month = String(date.getMonth() + 1).padStart(2, '0');
      var year = date.getFullYear();
      return day + "/" + month + "/" + year;
    };

    // Define FAQ items
    var faqs = [{
      theme: "general",
      question: "Quel est le prix d'une tiny house fabriqu\xE9e par " + commercial_name + " ?",
      answer: "Le prix d'une tiny house de " + commercial_name + " varie en fonction du mod\xE8le et des options choisies. Contactez-nous pour obtenir un devis personnalis\xE9."
    }, {
      theme: "general",
      question: "Comment financer une tiny house de " + commercial_name + " ?",
      answer: "Il existe plusieurs options pour financer l'achat de votre tiny house de " + commercial_name + ". Vous pouvez opter pour un pr\xEAt personnel, ou encore le financement participatif."
    }, {
      theme: "general",
      question: "Quels services propose " + commercial_name + " ?",
      answer: commercial_name + " propose une gamme compl\xE8te de services incluant la conception personnalis\xE9e, la fabrication sur mesure, l'installation et la livraison, le service apr\xE8s-vente, et des conseils en am\xE9nagement."
    }, {
      theme: "general",
      question: "Pourquoi choisir une tiny house " + commercial_name + " ?",
      answer: "Les tiny houses de " + commercial_name + " sont r\xE9put\xE9es pour leur qualit\xE9 sup\xE9rieure, leur durabilit\xE9 et leur design innovant."
    }, {
      theme: "delivery",
      question: "J'habite un autre d\xE9partement, est-ce que " + commercial_name + " peut me livrer ?",
      answer: commercial_name + " est bas\xE9 dans le " + department_name + " (" + department_code + "), mais nous pouvons livrer votre tiny house partout en France ! Contactez-nous pour \xE9tablir un devis personnalis\xE9 et organiser la livraison de votre future maison."
    }, {
      theme: "delivery",
      question: "O\xF9 peut-on visiter une tiny house de " + commercial_name + " pr\xE8s de chez moi ?",
      answer: "Vous pouvez visiter une tiny house " + commercial_name + " dans notre atelier de fabrication. Contactez-nous pour prendre rendez-vous. Vous pouvez \xE9galement essayer une de nos tiny houses en la louant via la page 'S\xE9jour en tiny house'."
    }, {
      theme: "delivery",
      question: "Quelle est l'adresse et le d\xE9partement de " + commercial_name + " ?",
      answer: address + " - " + department_name + " (" + department_code + ")"
    }, {
      theme: "quality",
      question: "Quels mat\xE9riaux utilise " + commercial_name + " pour ses tiny houses ?",
      answer: commercial_name + " utilise des mat\xE9riaux \xE9cologiques et durables pour la construction de ses tiny houses, garantissant ainsi une faible empreinte carbone et une grande long\xE9vit\xE9."
    }, {
      theme: "quality",
      question: "Quelle est la dur\xE9e de vie d'une tiny house construite par " + commercial_name + " ?",
      answer: "Les tiny houses de " + commercial_name + " sont con\xE7ues pour durer de nombreuses ann\xE9es gr\xE2ce \xE0 l'utilisation de mat\xE9riaux de haute qualit\xE9 et \xE0 une construction robuste."
    }, {
      theme: "quality",
      question: "Quels sont les avantages d'une tiny house " + commercial_name + " ?",
      answer: "Les tiny houses offrent de nombreux avantages : moins ch\xE8res \xE0 financer et entretenir qu'une maison traditionnelle, elles offrent les m\xEAmes avantages en termes d'intimit\xE9 et d'espace personnel. Elles n\xE9cessitent moins d'entretien et peuvent g\xE9n\xE9rer des revenus en \xE9tant lou\xE9es."
    }, {
      theme: "models",
      question: "Quels mod\xE8les de tiny houses propose " + commercial_name + " ?",
      answer: commercial_name + " offre une gamme vari\xE9e de mod\xE8les de tiny houses, des designs compacts et fonctionnels aux mod\xE8les plus spacieux et luxueux."
    }, {
      theme: "models",
      question: "Comment personnaliser une tiny house de " + commercial_name + " selon ses besoins ?",
      answer: commercial_name + " offre de nombreuses options de personnalisation pour r\xE9pondre aux besoins sp\xE9cifiques de chaque client, des plans de sol aux finitions int\xE9rieures."
    }];

    // Filter FAQs based on selected theme
    var filteredFaqs = faqs.filter(function (faq) {
      return faq.theme === _this.selectedTheme;
    });
    return m("div", {
      "class": "faq-container"
    }, m("div", {
      "class": "faq-header"
    }, [m("h2", {
      "class": "faq-title"
    }, "Questions fréquentes"), m("div", {
      "class": "filter-select-container"
    }, [m("select", {
      "class": "filter-select",
      value: this.selectedTheme,
      onchange: function onchange(e) {
        return _this.setSelectedTheme(e.target.value);
      }
    }, [m("option", {
      value: "general"
    }, "Général"), m("option", {
      value: "delivery"
    }, "Livraison"), m("option", {
      value: "quality"
    }, "Fabrication"), m("option", {
      value: "models"
    }, "Modèles")]), m("i", {
      "class": "fa fa-chevron-down filter-icon"
    })])]), m("div", {
      "class": "faq-items"
    }, filteredFaqs.map(function (faq, index) {
      var isExpanded = _this.expandedIndex === index;
      return m("div", {
        "class": "faq-item " + (isExpanded ? 'expanded' : '')
      }, [m("div", {
        "class": "faq-question",
        onclick: function onclick() {
          return _this.toggle(index);
        }
      }, [m("span", faq.question), m("i", {
        "class": "fa fa-chevron-" + (isExpanded ? 'up' : 'down')
      })]), isExpanded && m("p", {
        "class": "faq-answer"
      }, faq.answer)]);
    })));
  };
  return FAQComponent;
}((flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/components/FilterBar.js":
/*!*******************************************!*\
  !*** ./src/forum/components/FilterBar.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ FilterBar)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/common/Component */ "flarum/common/Component");
/* harmony import */ var flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_common_Component__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_common_components_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/common/components/Button */ "flarum/common/components/Button");
/* harmony import */ var flarum_common_components_Button__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_common_components_Button__WEBPACK_IMPORTED_MODULE_2__);



var FilterBar = /*#__PURE__*/function (_Component) {
  function FilterBar() {
    return _Component.apply(this, arguments) || this;
  }
  (0,_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(FilterBar, _Component);
  var _proto = FilterBar.prototype;
  _proto.view = function view() {
    return m('div', {
      "class": 'filter-bar'
    }, [
      //m("button", "barre filtre")
    ]);
  };
  return FilterBar;
}((flarum_common_Component__WEBPACK_IMPORTED_MODULE_1___default()));


/***/ }),

/***/ "./src/forum/index.js":
/*!****************************!*\
  !*** ./src/forum/index.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var flarum_common_extend__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flarum/common/extend */ "flarum/common/extend");
/* harmony import */ var flarum_common_extend__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(flarum_common_extend__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/forum/app */ "flarum/forum/app");
/* harmony import */ var flarum_forum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_forum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_FabricantListPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/FabricantListPage */ "./src/forum/components/FabricantListPage.js");
/* harmony import */ var _components_FabricantShowPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/FabricantShowPage */ "./src/forum/components/FabricantShowPage.js");
/* harmony import */ var _components_Card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Card */ "./src/forum/components/Card.js");
/* harmony import */ var _components_ContactCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/ContactCard */ "./src/forum/components/ContactCard.js");
// js/src/forum/index.js






flarum_forum_app__WEBPACK_IMPORTED_MODULE_1___default().initializers.add('top-vote', function () {
  (flarum_forum_app__WEBPACK_IMPORTED_MODULE_1___default().routes)['fabricants.list'] = {
    path: '/fabricants',
    component: _components_FabricantListPage__WEBPACK_IMPORTED_MODULE_2__["default"]
  };
  (flarum_forum_app__WEBPACK_IMPORTED_MODULE_1___default().routes)['fabricants.show'] = {
    path: '/fabricants/:slug',
    component: _components_FabricantShowPage__WEBPACK_IMPORTED_MODULE_3__["default"]
  };
});

/***/ }),

/***/ "flarum/common/Component":
/*!*********************************************************!*\
  !*** external "flarum.core.compat['common/Component']" ***!
  \*********************************************************/
/***/ ((module) => {

"use strict";
module.exports = flarum.core.compat['common/Component'];

/***/ }),

/***/ "flarum/common/components/Button":
/*!*****************************************************************!*\
  !*** external "flarum.core.compat['common/components/Button']" ***!
  \*****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = flarum.core.compat['common/components/Button'];

/***/ }),

/***/ "flarum/common/components/Page":
/*!***************************************************************!*\
  !*** external "flarum.core.compat['common/components/Page']" ***!
  \***************************************************************/
/***/ ((module) => {

"use strict";
module.exports = flarum.core.compat['common/components/Page'];

/***/ }),

/***/ "flarum/common/extend":
/*!******************************************************!*\
  !*** external "flarum.core.compat['common/extend']" ***!
  \******************************************************/
/***/ ((module) => {

"use strict";
module.exports = flarum.core.compat['common/extend'];

/***/ }),

/***/ "flarum/forum/app":
/*!**************************************************!*\
  !*** external "flarum.core.compat['forum/app']" ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
module.exports = flarum.core.compat['forum/app'];

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js ***!
  \******************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ _inheritsLoose)
/* harmony export */ });
/* harmony import */ var _setPrototypeOf_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./setPrototypeOf.js */ "./node_modules/@babel/runtime/helpers/esm/setPrototypeOf.js");

function _inheritsLoose(t, o) {
  t.prototype = Object.create(o.prototype), t.prototype.constructor = t, (0,_setPrototypeOf_js__WEBPACK_IMPORTED_MODULE_0__["default"])(t, o);
}


/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/setPrototypeOf.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/setPrototypeOf.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ _setPrototypeOf)
/* harmony export */ });
function _setPrototypeOf(t, e) {
  return _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) {
    return t.__proto__ = e, t;
  }, _setPrototypeOf(t, e);
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!******************!*\
  !*** ./forum.js ***!
  \******************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_forum__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/forum */ "./src/forum/index.js");

})();

module.exports = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=forum.js.map