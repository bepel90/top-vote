const flarumWebpackConfig = require('flarum-webpack-config');
const path = require('path');

module.exports = flarumWebpackConfig({
  useExtensions: ['ts', 'tsx'],
  resolve: {
    alias: {
      'flarum': path.resolve(__dirname, '../flarum/core/js/src'),
      'flarum/forum': path.resolve(__dirname, '../flarum/core/js/src/forum'),
      'flarum/common': path.resolve(__dirname, '../flarum/core/js/src/common'),
    }
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'less-loader'
        ]
      }
    ]
  }
});
