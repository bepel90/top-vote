<?php

use Flarum\Extend;
use Bepel90\TopVote\Api\Controller\ListDepartmentsController;
use Bepel90\TopVote\Api\Controller\ShowFabricantController;
use Bepel90\TopVote\Api\Controller\ListFabricantsController;
use Bepel90\TopVote\Api\Controller\ListFabricantsByDepartmentController;
use Bepel90\TopVote\Api\Controller\LogFabricantClickController;
use Bepel90\TopVote\Content\DepartmentListController;
use Bepel90\TopVote\Middleware\LogFabricantClickMiddleware;
use Flarum\Frontend\Document;

return [
    // Ajoutez les locales si nécessaire
    // new Extend\Locales(__DIR__.'/locale'),

    (new Extend\Frontend('admin'))
        ->js(__DIR__.'/js/dist/admin.js'),

    (new Extend\Frontend('forum'))
        ->js(__DIR__.'/js/dist/forum.js')
        ->css(__DIR__.'/less/styles.less'),

    (new Extend\Routes('api'))
        ->get('/departements', 'departments.index', ListDepartmentsController::class)
        ->get('/fabricants', 'fabricants.index', ListFabricantsController::class)
        ->get('/fabricants/{slug}', 'fabricants.show', ShowFabricantController::class),
        //->get('/fabricants/departement/{code}', 'fabricants.department', ListFabricantsByDepartmentController::class)
        //->post('/fabricants/{id}/click', 'fabricants.click', LogFabricantClickController::class),

    (new Extend\Frontend('forum'))
        ->route('/fabricants', 'fabricants.list')
        ->route('/fabricants/{slug}', 'fabricants.show'),
];